# Spring Boot Java Web App Creates Message on Redis Server

## Introduction

Sample Java Web App
- Create Message on Redis
- Search Message per User

Redis Cached Message Data Model
- Data Type: Key - Hash
- Key: User
- Hash: id(Hash Key) - message(Hash Value)

## JDK

JDK 1.8 is used

Please change the pom.xml if you want to change JDK version


## Launch Web App

Go to project root directory

    $ mvn spring-boot:run

## Build App

    $ mvn clean install

## Build Docker Image

    $ docker build -t chris/springboot:1.0 .

## Run Docker Container

    $ docker run --name redis-app -d -p 8080:8080 -t chris/springboot:1.0

# Backend

## Configuration

RedisConfig defines
- Jedis (Redis Client)
- Redis Template (library for redis operations (CRUD))

## Entity

Not in used as we won't store Java Object into Redis directly

## Controller

RedisController performs Create and Search operations

## Service

RedisService defines CRUD operations

## Redis Connection Properties

Please find Redis connection param from application.properties



# Frontend

## Static Content

Please place CSS, Images under resources/static

## View

Please place html under resources/templates

Spring Boot Thymeleaf is used to receive param from Controller


