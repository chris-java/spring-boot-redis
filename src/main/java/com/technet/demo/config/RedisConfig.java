package com.technet.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.HashMap;


@ComponentScan
@EnableCaching
@Configuration
public class RedisConfig extends CachingConfigurerSupport {

    // Get Redis Host from application.properties
    @Value(value = "${spring.redis.host}")
    private String redisHost;

    // Get Redis Port from application.properties
    @Value(value = "${spring.redis.port}")
    private int redisPort;

    // Jedis Connection must be public
    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(redisHost);
        redisStandaloneConfiguration.setPort(redisPort);
        return new JedisConnectionFactory(redisStandaloneConfiguration);
    }

    // Redis Template is used to perform CRUD operations on Redis
    @Bean
    public RedisTemplate redisTemplate() {
        RedisTemplate<String, HashMap> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        // Solve the issue caused by Java Serialization e.g. key: \xac\xed\x00\x05t\x00\<key>
        template.setKeySerializer(RedisSerializer.string());
        // Solve the issue caused by Java Serialization e.g. value: \xac\xed\x00\x05t\x00\<value>
        template.setHashKeySerializer(RedisSerializer.string());
        template.setHashValueSerializer(RedisSerializer.string());
        return template;
    }
}
