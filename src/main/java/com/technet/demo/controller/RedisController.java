package com.technet.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.technet.demo.service.RedisService;


@Controller
public class RedisController {

    @Autowired
    private RedisService redisService;

    @GetMapping("/redis")
    public String listMessage(Model model) {
        return "redis/main";
    }

    @GetMapping("/redis/searchMessage")
    public String getMessage(@RequestParam String searchUser, Model model) {
        model.addAttribute("searchUser", searchUser);
        model.addAttribute("userMessages", redisService.getMessageByUser(searchUser));
        return "redis/userMessage";
    }

    @PostMapping("/redis/createMessage")
    public String createMessage(@RequestParam String user, @RequestParam String content) {
        redisService.create(user, content);
        return "redirect:/redis";
    }
}
