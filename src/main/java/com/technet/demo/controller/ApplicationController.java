package com.technet.demo.controller;

import java.util.HashMap;

import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class ApplicationController {
    // Access param in application.properties
    @Value(value = "${spring.redis.host}")
    private String redisHost;

    @Value(value = "${spring.redis.port}")
    private String redisPort;

    // Put params into model in order to be shown on thymeleaf view html
    @GetMapping("/")
    public String index(Model model) {
        HashMap<String, String> redisInfo = new HashMap<>();
        redisInfo.put("Host", redisHost);
        redisInfo.put("Port", redisPort);
        model.addAttribute("redisInfo", redisInfo);
        return "index";
    }

}
