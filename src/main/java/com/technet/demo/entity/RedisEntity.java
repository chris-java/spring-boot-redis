package com.technet.demo.entity;

import java.io.Serializable;

public class RedisEntity implements Serializable {
    private static final long serialVersionUID = -305726463442998985L;

    private String id;

    private String user;

    private String content;

    // Getter and Setter of Redis Message Attributes
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }
}
