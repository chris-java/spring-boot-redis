package com.technet.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.technet.demo.entity.RedisEntity;


@Service
public class RedisService {

    @Autowired
    private RedisTemplate<String, HashMap> redisTemplate;

    public Map<String, String> getMessageByUser(final String user) {
        return redisOperations().entries(user);
    }

    public void create(final String user, final String content) {
        redisOperations().put(user, UUID.randomUUID().toString(), content);
    }

    public void delete(final String user, final String id) {
        redisOperations().delete(user, id);
    }

    // Base function to perform CRU (no D) Operations via ValueOperations
    private HashOperations<String, String, String> redisOperations() {
        return redisTemplate.opsForHash();
    }
}
